//
//  AppDelegate.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 26/12/17.
//  Copyright © 2017 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import Foundation
import MapKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
      var isFirstTime = true
    var window: UIWindow?
    var locManager = CLLocationManager()
  

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        isFirstTime = false

        print(Tools.getHash())
        
        UIApplication.shared.statusBarStyle = .lightContent

        locManager.requestWhenInUseAuthorization()
        
        if isFirstTime == false {
            if SavedData.getTheUserId() == 0 {
                print("No hay sesión iniciada")
                let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
                
            } else {
                print("Si hay sesión iniciada")
                /*let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController : UITabBarController = mainStoryboardIpad.instantiateViewController(withIdentifier: "TabBarController") as!  UITabBarController
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
                 */}
        }
       
        
        var currentLocation: CLLocation!
        
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            
            currentLocation = locManager.location
            SavedData.setTheLatitude(double: currentLocation.coordinate.latitude)
            SavedData.setTheLongitude(double: currentLocation.coordinate.longitude)
            
            
        }
        
SavedData.setSecretKey(secretKey: "53f439a00cb47eb2ec775531a7592b2b")
        let screenSiz: CGRect = UIScreen.main.bounds
      print("Este es el tamaño de la pantalla", screenSiz)

        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal)
     
        return true
    }
 
    func applicationWillResignActive(_ application: UIApplication) {

    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

