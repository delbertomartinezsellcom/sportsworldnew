//
//  RegisterViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 10/01/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var cancelarButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var phoneNumber = "018000079727"
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelarButton.layer.cornerRadius = 8
        activityIndicator.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    //MARK:- BUTTONS
    @IBAction func clickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
      
    }
    
    @IBAction func clickRegistrarButton(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            registerUser()

        } else {
            Alert.ShowAlert(title: "ATENCIÓN", message: "No cuentas con internet, verifica tu conexión o intenta mas tarde.", titleForTheAction: "ACEPTAR", in: self)
        }
        
    }
    
    func registerUser() {
    
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        view.isUserInteractionEnabled = false
        APIManager.sharedInstance.register(email: emailRegister, tipo: typeInvited, club: finalIdClub, membresia: memberRegister, id: memberRegister, onSuccess: {json in
            DispatchQueue.main.async {
            if APIManager.sharedInstance.codeMessage >= 400 {
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
                let alertController = UIAlertController(title: "¡ATENCIÓN!", message: APIManager.sharedInstance.message, preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "ACEPTAR", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                  self.goToThePhone()
                }
               
                alertController.addAction(okAction)
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }else if APIManager.sharedInstance.codeMessage == 200 {
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                self.view.isUserInteractionEnabled = true
                
                let alertController = UIAlertController(title: "¡ATENCIÓN!", message: APIManager.sharedInstance.message, preferredStyle: .alert)
                
                    let okAction = UIAlertAction(title: "ACEPTAR", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                        self.returnToLogin()
                }
                // Add the actions
                alertController.addAction(okAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
          }
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        }, onFailure: { error in
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            let alert = UIAlertController(title: "ERROR", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.show(alert, sender: nil)
        })
    }
    
    func goToThePhone() {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }

    }
    

    func showActivityIndicator() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    func hideActivityIndicator() {
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }
    func returnToLogin() {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController!.pushViewController(VC1, animated: true)

    }
}
