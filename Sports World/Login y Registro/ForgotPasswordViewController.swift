//
//  ForgotPasswordViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 10/01/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var cancelarButton: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        activity.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ForgotPasswordViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
      cancelarButton.layer.cornerRadius = 8
        
    }
    @IBAction func clickCancelarButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func recoverPasseword() {
        activity.isHidden = false
        activity.startAnimating()
        view.isUserInteractionEnabled = false
        APIManager.sharedInstance.recoverPassword(email: emailForRecoverPassword, onSuccess: { json in
            DispatchQueue.main .async {
                print("Esto es lo que se va", emailForRecoverPassword)
                if APIManager.sharedInstance.codeMessage == 200 {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    Alert.ShowAlert(title: "ATENCIÓN", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
                } else {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                     Alert.ShowAlert(title: "ATENCIÓN", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
                }
                
                }
                        
        }, onFailure: { error in
            self.activity.isHidden = true
            self.activity.stopAnimating()
            self.view.isUserInteractionEnabled = true
          Alert.ShowAlert(title: "ATENCIÓN", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
        })
    }
    @IBAction func clickRecuperarButton(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            recoverPasseword()
        } else {
            Alert.ShowAlert(title: "ATENCIÓN", message: "No cuentas con internet, verifica tu conexión o intenta mas tarde.", titleForTheAction: "ACEPTAR", in: self)
        }
    }
    
}
