//
//  CategoriasViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 4/30/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class CategoriasViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate {
    @IBOutlet weak var tabView: UITableView!
    
    var searchController: UISearchController!
    
    var categoryClasses = ["Dance","Wellness","Strength","Intensity","Contact","Kids"]
    
    var categoryClasesFiltered = [String]()

    var filterData: [String] = []
    @IBOutlet weak var barSearch: UISearchBar!
    var isSearching = false
    //CONECTA LOS OUTLETS.
    
    @IBOutlet weak var searchBar: UISearchBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

       //CONFIGURA LA SEARCH BAR
       
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CategoriasViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.barSearch.delegate = self
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    //MARK:- TableView
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor.clear
        let headerLabel = UILabel(frame: CGRect(x: 30, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "LarkeNeueBold-Bold", size: 20)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return categoryClasses.count
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return  categoryClasses[section]
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriasTableViewCell", for: indexPath) as! CategoriasTableViewCell
        cell.collectionView.reloadData()
        
        return cell
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
     
     
    }
   
}
extension CategoriasViewController: UICollectionViewDelegate,  UICollectionViewDataSource  {
 
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriasCollectionViewCell", for: indexPath) as! CategoriasCollectionViewCell
        cell.borderedView.backgroundColor = UIColor.clear
        cell.borderedView.layer.cornerRadius = 20.0
        cell.borderedView.layer.borderColor = UIColor.red.cgColor
        cell.borderedView.layer.borderWidth = 1.0
        
        if isSearching {
            cell.classCategoryName.text = categoryClasesFiltered[indexPath.row]
        }else {
            cell.classCategoryName.text = categoryClasses[indexPath.row]
        }
        return cell
    }
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isSearching {
            
            return self.categoryClasesFiltered.count
        } else {
            
        return self.categoryClasses.count

            }
        }
        
   
}

   
    
  

