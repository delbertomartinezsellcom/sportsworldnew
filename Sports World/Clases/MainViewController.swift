//
//  MainViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 2/27/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
class MainViewController: UIViewController, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource  {
    private var cell = "MainCollectionViewCell"
    
    @IBAction func picker(_ sender: Any) {
        print("*")
        self.picker.alpha = 1
        picker.isHidden = false
    }
    
    var arrayOfDays = ["1", "2","3","4", "5"]
    
    let items = ["pizza", "deep dish pizza", "calzone"]
    
    let titles = ["Margarita", "BBQ Chicken", "Pepperoni", "sausage", "meat lovers", "veggie lovers", "sausage", "chicken pesto", "prawns", "mushrooms"]
    var currentSelected = Date()
    let formatter = DateFormatter()
    
    @IBOutlet weak var localNavigationItem: UINavigationItem!
    @IBOutlet weak var clubName: UILabel!
    @IBOutlet weak var colleView: UICollectionView!
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var categoriasView: UIView!
    @IBOutlet weak var informacionView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    //Termina la configuración del calendario.
    var days = [Int]()
    var daysDateFormat = [Date]()
    var currentClub : Club = Club()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIManager.sharedInstance.mainClassViewController = self
        tabView.delegate = self
        tabView.dataSource = self
        colleView.delegate = self
        colleView.dataSource = self
        
        colleView.isUserInteractionEnabled = true
        formatter.dateFormat = "dd"
        
        let result = formatter.string(from: currentSelected)
        print("date", result)
        
        let cal = Calendar.current
        var date = cal.startOfDay(for: Date())
        
        self.currentSelected = date
        var move = 0
        date = cal.date(byAdding: .day, value: -10, to: date)!
        for i in 1 ... 20 {
            let day = cal.component(.day, from: date)
            days.append(day)
            daysDateFormat.append(date)
            if(self.currentSelected == date){
                move = i - 1
            }
            date = cal.date(byAdding: .day, value: +1, to: date)!
            
        }
        
        print("Dias", days)
        print("Dias", days)
        
        
        
        //Customiza la navigationBar para que sea transparente.
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //Configura las vistas para mostrar las vistas del segmented
        
        informacionView.isHidden = true
        categoriasView.isHidden = true
        
        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        
        
        self.currentClub = (APIManager.sharedInstance.allClubes.sorted(by: { $0.distance < $1.distance }).first)!
        APIManager.sharedInstance.selectedClub = self.currentClub
        self.clubName.text = self.currentClub.name
        
        //let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.dismissKeyboard))
        //view.addGestureRecognizer(tap)
        
        let pickerRect = CGRect(x: 0, y: 64, width: self.view.bounds.width, height: self.view.bounds.height - 64)
        
        
        picker.frame = pickerRect
        picker.backgroundColor = UIColor.lightGray
        picker.setValue(UIColor.white, forKeyPath: "textColor")
        
        picker.delegate = self
        picker.dataSource = self
        picker.isHidden = true
        view.addSubview(picker)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            let indexPath : IndexPath = IndexPath(row: move, section: 0)
            
            self.colleView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        })
        
    }
    
    var hours : [String] = [String]()
    var localClasss : [ClassSW] = [ClassSW]()
    func updateCalendar(){
        DispatchQueue.main.async {
            //self.clubName.text = self.currentClub.name
            APIManager.sharedInstance.infoVC.updateValues()
            self.hours = []
            self.tabView.reloadData()
        }
        APIManager.sharedInstance.getClasessByClub(date:self.currentSelected,idClub: APIManager.sharedInstance.selectedClub.clubId, onSuccess: { classArray in
            DispatchQueue.main.async {
                print(classArray)
                self.localClasss = classArray.sorted(by: {$0.clase < $1.clase})
                self.hours = uniq(source: classArray.map { $0.inicio}).sorted()
                self.tabView.reloadData()
            }
            
        }, onFailure: { error in
            
        })
    }
    
    func getTheClubes() {
        APIManager.sharedInstance.getClubesForRegister(onSuccess: { json in
            
            DispatchQueue.main.async {
                
            }
        }, onFailure: { error in
            
        })
        
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        return NSAttributedString(string: APIManager.sharedInstance.allClubes[row].name, attributes: [NSAttributedStringKey.foregroundColor:UIColor.white])
    }
    var picker = UIPickerView()
    @IBAction func lblTapped(sender: UITapGestureRecognizer) {
        print("*")
        picker.isHidden = false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return APIManager.sharedInstance.allClubes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.clubName.text = APIManager.sharedInstance.allClubes[row].name
        //self.picker.isHidden = true
        self.currentClub = APIManager.sharedInstance.allClubes[row]
        APIManager.sharedInstance.selectedClub = self.currentClub
        self.updateCalendar()
        APIManager.sharedInstance.infoVC.updateValues()
        UIView.animate(withDuration: 1.0, animations: {
            self.picker.alpha = 0
        }) { (finished) in
            self.picker.isHidden = true
            self.view.endEditing(true)
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return APIManager.sharedInstance.allClubes[row].name
    }
    
    deinit {
        print("\(#function)")
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.htmlString = ""
        self.selectedTag = 0
        if(!self.comesFromWebView){
        self.getTheClubes()
        segmentedControl.selectedSegmentIndex = 1
        self.clubName.text = APIManager.sharedInstance.selectedClub.name
        self.updateCalendar()
        }else{
            
        }
        self.comesFromWebView = false
        
    }
    //MARK:- CONFIGURA LA VISTA DEL CALENDARIO.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: cell, for: indexPath as IndexPath) as! MainCollectionViewCell
        cel.dayNumberLabel.text = String(days[indexPath.row])
        
        
        
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "es_MX") as Locale!
        dateFormatter.dateFormat = "EEE"
        cel.dayLabel.text =  dateFormatter.string(from: self.daysDateFormat[indexPath.row])
        
        if(self.currentSelected.compare(self.daysDateFormat[indexPath.row]) == ComparisonResult.orderedSame ){
            
            cel.dayNumberLabel.layer.borderWidth = 2.0
            cel.dayNumberLabel.layer.borderColor = UIColor.white.cgColor
            cel.dayNumberLabel.layer.cornerRadius = cel.dayNumberLabel.bounds.width / 2
            
        }else{
            //cel.dayNumberLabel.textColor = UIColor.white
            
            cel.dayNumberLabel.layer.borderWidth = 0.0
            cel.dayNumberLabel.layer.borderColor = UIColor.clear.cgColor
            cel.dayNumberLabel.layer.cornerRadius = 0
        }
        return cel
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentSelected = self.daysDateFormat[indexPath.row];
        for i in 0...(self.colleView.visibleCells.count - 1){
            let tempCell : MainCollectionViewCell = self.colleView.visibleCells[i] as! MainCollectionViewCell
            
            
            //tempCell.dayNumberLabel.textColor = UIColor.white
            
            tempCell.dayNumberLabel.layer.borderWidth = 0.0
            tempCell.dayNumberLabel.layer.borderColor = UIColor.clear.cgColor
            tempCell.dayNumberLabel.layer.cornerRadius = 0
            
        }
        let tempCellSelect : MainCollectionViewCell = self.colleView.cellForItem(at: indexPath) as! MainCollectionViewCell
        //tempCellSelect.dayNumberLabel.textColor = UIColor.red
        tempCellSelect.dayNumberLabel.layer.borderWidth = 2.0
        tempCellSelect.dayNumberLabel.layer.borderColor = UIColor.white.cgColor
        tempCellSelect.dayNumberLabel.layer.cornerRadius = tempCellSelect.dayNumberLabel.bounds.width / 2
        self.colleView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        self.updateCalendar()
    }
    
    //MARK:- BOTONES
    
    @IBAction func clickMenuButton(_ sender: Any) {
        DispatchQueue.main.async {
            let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
            self.navigationController!.pushViewController(VC1, animated: true)
        }
    }
    
    @IBAction func clickMapaButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "ClubesViewController") as! ClubesViewController
        self.navigationController!.pushViewController(VC1, animated: true)
    }
    @IBAction func clickSegment(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            informacionView.isHidden = false
            categoriasView.isHidden = true
            tabView.isHidden = true
            
        } else if sender.selectedSegmentIndex == 1 {
            informacionView.isHidden = true
            categoriasView.isHidden = true
            tabView.isHidden = false
            
        } else if sender.selectedSegmentIndex == 2 {
            informacionView.isHidden = true
            categoriasView.isHidden = false
            tabView.isHidden = true
            
        }
    }
    //MARK:- TableView
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor.clear
        let headerLabel = UILabel(frame: CGRect(x: 30, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "LarkeNeueBold-Bold", size: 20)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.hours.count //titles.count
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dateAsString = self.hours[section]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: date!)
    }
    
    
    //  func sectionIndexTitles(for tableView: UITableView) -> [String]? {
    //    return titles
    // }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.localClasss.filter({$0.inicio == self.hours[section]}).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClasesTableViewCell", for: indexPath) as! ClasesTableViewCell
        
        let tempClass = self.localClasss.filter({$0.inicio == self.hours[indexPath.section]})[indexPath.row]
        
        cell.tag = tempClass.idclase
        cell.nameLabel.text! = tempClass.clase
        cell.instructorName.text! = tempClass.instructor
        
        if(tempClass.inscrito){
            cell.nameLabel?.textColor = UIColor.red
            cell.instructorName?.textColor = UIColor.red
            
        }else{
            cell.nameLabel?.textColor = UIColor.white
            cell.instructorName?.textColor = UIColor.white
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("touch")
        let tempClass = self.localClasss.filter({$0.inicio == self.hours[indexPath.section]})[indexPath.row]
        let params : [String:Any] = ["classdate": tempClass.iniciovigencia,
                                     "user_id" : SavedData.getTheUserId(),
                                     "employed_id" :0,
                                     "origin" : 4,
                                     "confirm" : 0,
                                     "idconfirm" : 1,
                                     "idinstactprg" : tempClass.idinstalacionactividadprogramada]
        if(!tempClass.inscrito){
            APIManager.sharedInstance.makeReservation(params: params , onSuccess: { result in
                
                DispatchQueue.main .async {
                    
                    
                    if(result.status == true){
                        self.localClasss.filter({$0.inicio == self.hours[indexPath.section]})[indexPath.row].inscrito = true
                        let cell = tableView.cellForRow(at: indexPath) as!  ClasesTableViewCell
                        //cell.backgroundColor = UIColor.red
                        cell.nameLabel?.textColor = UIColor.red
                        cell.instructorName?.textColor = UIColor.red
                        Alert.ShowAlert(title: "Reservación", message: "Tu clase ha sido reservada", titleForTheAction: "Aceptar", in: self)
                    }else{
                        Alert.ShowAlert(title: "Reservación", message: result.message, titleForTheAction: "Aceptar", in: self)
                    }
                    
                    
                }
            }, onFailure: { error in
                
            })
        }else{
            /*
             
             let params = [
             "personaact": Int(user!.userId),
             "fechahora": dateTime,
             "idclub": "0",
             "idsalon": classe["idsalon"].intValue,
             "idclase": classe["idclase"].intValue,
             "usuario": "mefistoxxx",
             "instalacionactprogramada": classe["idinstalacionactividadprogramada"].intValue
             ] as [String : AnyObject]*/
            let dateTime = String(format: "%@ %@",
                                  tempClass.iniciovigencia,
                                  tempClass.inicio)
            
            let params = [
                "personaact": SavedData.getTheUserId(),
                "fechahora": dateTime,
                "idclub": "0",
                "idsalon": tempClass.idsalon,
                "idclase": tempClass.idclase,
                "usuario": "mefistoxxx",
                "instalacionactprogramada": tempClass.idinstalacionactividadprogramada
                ] as [String : AnyObject]
            APIManager.sharedInstance.cancelReservation(params: params , onSuccess: { result in
                
                DispatchQueue.main .async {
                    
                    if(result.status == true){
                        self.localClasss.filter({$0.inicio == self.hours[indexPath.section]})[indexPath.row].inscrito = false
                        let cell = tableView.cellForRow(at: indexPath) as!  ClasesTableViewCell
                        cell.nameLabel?.textColor = UIColor.white
                        cell.instructorName?.textColor = UIColor.white
                        Alert.ShowAlert(title: "Reservación", message: "Tu clase ha sido cancelada", titleForTheAction: "Aceptar", in: self)
                    }else{
                        Alert.ShowAlert(title: "Reservación", message: result.message, titleForTheAction: "Aceptar", in: self)
                    }
                    
                }
            }, onFailure: { error in
                
            })
        }
    }
    func loadWebView(){
        self.performSegue(withIdentifier: "showWebView", sender: nil)
    }
    var htmlString : String = String()
    var selectedTag : Int = 0
    var comesFromWebView : Bool = Bool()
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showWebView"){
            let vc : WebVC = (segue.destination as? WebVC)!
            vc.htmlString = self.htmlString
            self.comesFromWebView = true
            vc.titlesString = (self.localClasss.filter({$0.idclase == self.selectedTag}).first?.clase)!
        }
    }
}

