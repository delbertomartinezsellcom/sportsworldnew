//
//  PagoViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 4/11/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import SwiftyJSON
class PagoViewController: UIViewController, UITextFieldDelegate{
    var currentTextField: UITextField?
    
    @IBOutlet weak var cardHolderName: UITextField!
    @IBOutlet weak var cardMonth: UITextField!
    @IBOutlet weak var cardNumber: UITextField!
    @IBOutlet weak var cardYear: UITextField!
    @IBOutlet weak var cardCvv: UITextField!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activity.isHidden = true
        
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PagoViewController.dismissKeyboard))
    view.addGestureRecognizer(tap)

    }

     func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextField?.resignFirstResponder()
        textField.resignFirstResponder()
        currentTextField = nil
    }
    func validateTextFields () -> Bool {
        if cardHolderName.text == nil  && cardMonth.text == nil && cardNumber.text == nil && cardYear.text == nil  && cardCvv.text == nil {
            Alert.ShowAlert(title: "ATENCIÓN", message: "Verifica que tus campos esten llenos antes de continuar", titleForTheAction: "ACEPTAR", in: self)
            return false
        } else {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == cardHolderName) {
            cardMonth.becomeFirstResponder()
        }else if(textField == cardMonth) {
            cardYear.becomeFirstResponder()
        }else if(textField == cardYear) {
            cardNumber.becomeFirstResponder()
        } else if (textField == cardNumber) {
            cardCvv.becomeFirstResponder()
        }else if (textField == cardCvv) {
            currentTextField?.resignFirstResponder()
        }
        return true
    }
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.tag == 2 {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField.tag == 3 {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
             return newString.length <= maxLength
        } else if textField.tag == 4 {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField.tag == 5 {
                let maxLength = 3
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
        }
       return true
    }
    
        func conekta() {
            activity.isHidden = false
            activity.startAnimating()
            view.isUserInteractionEnabled = false
            let conekta = Conekta()
            
            conekta.delegate = self
            
            conekta.publicKey = "key_KJysdbf6PotS2ut2"
            
            conekta.collectDevice()
            
            let card = conekta.card()
            
            card?.setNumber(/*cardNumber.text*/"4213166079278838", name: /*cardHolderName.text*/"Delberto Martinez Salazar", cvc: /*cardCvv.text*/"020", expMonth: /*cardMonth.text*/"12", expYear: /*cardYear.text*/"21")
            
            let token = conekta.token()
            
            token?.card = card
        
            token?.create(success: { (data) -> Void in
                
                var newData =  data! as NSDictionary
                var id = newData["id"] as? String
                SavedData.settokenConekta(tokenConekta: id ?? "")
                if SavedData.gettokenConekta() != "" {
                    self.makeThePayment()
                    
                } else {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                      Alert.ShowAlert(title: "¡ATENCIÓN!", message: "Hubo un error en tokenizar tu tarjeta, verifica tus datos e intenta nuevamente.", titleForTheAction: "ACEPTAR", in: self)
                }
            }, andError: { (error) -> Void in
                print(error)
            })
            
       }
    
    func makeThePayment() {
        APIManager.sharedInstance.conekta(token_id: "tok_2ifZZoGwqBZnApTYN", person_id: SavedData.getTheUserId(), name: SavedData.getTheName(), unit_price: 0, quantity: 0 , onSuccess: { json in
            
            DispatchQueue.main .async {
                
                self.activity.isHidden = true
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                Alert.ShowAlert(title: "ACEPTAR", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
            }
        }, onFailure: { error in
            self.activity.isHidden = true
            self.activity.stopAnimating()
            self.view.isUserInteractionEnabled = true
            Alert.ShowAlert(title: "ACEPTAR", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
        })
    }
    @IBAction func conektaButtoon(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
         makeThePayment()
        } else {
            Alert.ShowAlert(title: "ATENCIÓN", message: "No cuentas con conexión a internet, revisa tu conexión o intenta mas tarde.", titleForTheAction: "ACEPTAR", in: self)
        }
       
    }
    @objc func dismissKeyboard() {
    view.endEditing(true)
}
    
   

}
