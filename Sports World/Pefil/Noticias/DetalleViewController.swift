//
//  DetalleViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/15/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
class DetalleViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var tituloNoticiaLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var subtituloLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var imageV: UIImageView!
    
   var tituloFinal = ""
   var subtituloFinal = ""
   var descripcionNoticia = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        imageV.image = UIImage(named: "logo_SW_recuperar_contraseña")
        tituloNoticiaLabel.text = tituloFinal
        subtituloLabel.text = subtituloFinal
        webView.loadHTMLString(descripcionNoticia, baseURL: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        backView.layer.cornerRadius = 10
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        dismissKeyboard()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        
    }
   

}
