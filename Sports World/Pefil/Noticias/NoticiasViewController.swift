//
//  NoticiasViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/14/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class NoticiasViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var colleView: UICollectionView!
    var noticiasArray = APIManager.sharedInstance.noticaResumen
    var tituloNoticias = APIManager.sharedInstance.tituloArray
    var descripcionNoticias = APIManager.sharedInstance.descripcionArray
     var titulo2  = APIManager.sharedInstance.descripcionArray2
    var noticiaImagenString = APIManager.sharedInstance.imagenNoticia
    var noticiaImagen = [UIImage]()
    private let cell = "NoticiasCollectionViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        colleView.dataSource = self
        colleView.delegate = self
       
        
        let images = noticiaImagenString.flatMap{ link->UIImage? in
            guard let url = URL(string: link) else {return nil}
            guard let imageData = try? Data(contentsOf: url) else {return nil}
            var finalImage = UIImage(data: imageData)
            noticiaImagen.append(finalImage!)
            
            return UIImage(data: imageData)
            
        }
        
      
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }

     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
        return noticiasArray.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: cell, for: indexPath as IndexPath) as! NoticiasCollectionViewCell
        
        cel.descriptionNews.text = noticiasArray[indexPath.row]
        cel.tituloLabel.text = tituloNoticias[indexPath.row]
        cel.titleUnderImage.text = titulo2[indexPath.row]
        cel.imageToShow.image = UIImage(named: "logo_SW_recuperar_contraseña")
        
        //let imageURL = URL(string: noticiaImagenString[indexPath.row])
        //let data = try? Data(contentsOf: imageURL!)
        //var imageConverted = UIImage(data: data!)
        //noticiaImagen.append(imageConverted!)
        //cel.imageToShow.image = noticiaImagen[indexPath.row]
        cel.backView.layer.cornerRadius = 10
        
        return cel
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetalleViewController") as? DetalleViewController
        vc?.tituloFinal = tituloNoticias[indexPath.row]
        vc?.subtituloFinal = titulo2[indexPath.row]
        vc?.descripcionNoticia = descripcionNoticias[indexPath.row]
        //vc?.imageFinal = UIImage(named: "logo_SW_recuperar_contraseña")!
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func clickMenuButton(_ sender: Any) {
        
        DispatchQueue.main.async {
            let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
            self.navigationController!.pushViewController(VC1, animated: true)
        }
    }
   
  
    
}
