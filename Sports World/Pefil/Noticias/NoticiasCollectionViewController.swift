//
//  NoticiasCollectionViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/15/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

private let cell = "NoticiasCollectionViewCell"

class NoticiasCollectionViewController: UICollectionViewController {
    
    var arrayOfImages: [UIImage] = [#imageLiteral(resourceName: "gym_image"),#imageLiteral(resourceName: "gym_image"),#imageLiteral(resourceName: "gym_image"),#imageLiteral(resourceName: "gym_image"),#imageLiteral(resourceName: "gym_image"),#imageLiteral(resourceName: "gym_image"),#imageLiteral(resourceName: "gym_image")]

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return arrayOfImages.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: cell, for: indexPath as IndexPath) as! NoticiasCollectionViewCell
        cel.imageToShow.image = UIImage(named: "logo_SW_recuperar_contraseña")
        cel.descriptionNews.text = "Esta es la descripción"
        
        return cel
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("Cell selected")

    }
    
}
