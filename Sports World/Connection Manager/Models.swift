//
//  Models.swift
//  Sports World
//
//  Created by Glauco Valdes on 6/11/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import Foundation
open class Club{
    var name : String = ""
    var group : String = ""
    var clubId : Int = 0
    var distance : Double = 0.0
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var agenda : Int = 0
    var tele : String = ""
    var idEstado : Int = 0
    var address : String = ""
    var schedule : String = ""
    var clave : String = ""
    var dCount : Int = 0
    var ruta360 : String = ""
    var rutaVideo : String = ""
    var preventa : Int = 0
    var estado : String = ""
}

open class ClassSW{
    var idinstalacionactividadprogramada : Int = 0
    var club : String = ""
    var salon : String = ""
    var idsalon : Int = 0
    var clase : String = ""
    var idclase : Int = 0
    var instructor : String = ""
    var inicio : String = ""
    var fin : String = ""
    var iniciovigencia : String = ""
    var finvigencia : String = ""
    var capacidadideal : Int = 0
    var capacidadmaxima : Int = 0
    var capacidadregistrada : Int = 0
    var reservacion : Int = 0
    var confirmados : Int = 0
    var agendar : Int = 0
    var demand : Int = 0
    var inscrito : Bool = false
}
open class GenericResponse{
    var status : Bool = false
    var message : String = ""
}

open class DescriptionResponse{
    var status : Bool = false
    var message : String = ""
    var data : DescriptionData = DescriptionData()
}
open class DescriptionData{
    var idClass : Int = 0
    var descripcionHTML : String = ""
    var clave : String = ""
    
}

open class ClubResponse{
    var status : Bool = false
    var message : String = ""
    var data : ClubData = ClubData()
}
open class ClubData{
    var latitud : Double = 0
    var agendar : Int = 0
    var favorito : Bool = false
    var direccion : String = ""
    var horario : String = ""
    var idun : Int = 0
    var imagenes_club : [String] = [String]()
    var ruta360 : String = ""
    var nombre : String = ""
    var rutavideo : String = ""
    var preventa : Int = 0
    var telefono : String = ""
    var longitud : Double = 0
}
