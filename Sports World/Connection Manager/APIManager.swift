
import Foundation
import SwiftyJSON
import CoreLocation

class APIManager: NSObject {
    var mainClassViewController : MainViewController = MainViewController()
    var proximasAperturasVC : ProximasAperturasCollectionViewController = ProximasAperturasCollectionViewController()
    var favoritesVC : FavoritosCollectionViewController = FavoritosCollectionViewController ()
    var ciudadesVC : ForaneosCollectionViewController = ForaneosCollectionViewController ()
    
    var selectedClub : Club = Club()
    var allClubes : [Club] = [Club]()
    var allClubesInfo = [[String:AnyObject]]()
    var dictionaryP1 = [[String:AnyObject]]()
    var dictionaryP2 = [[String:AnyObject]]()
    var dictionaryP3 = [[String:AnyObject]]()
    var dictionaryP4 = [[String:AnyObject]]()
    var finalList  = [String]()
    var finalId = [Int]()
    var totalImportes = [String]()
    var totalDescripciones = [String]()
    var listClub = [[String:AnyObject]]()
    var listClubP1 = [[String:AnyObject]]()
    var listClubP2 = [[String:AnyObject]]()
    var listClubP3 = [[String:AnyObject]]()
    var listClubP4 = [[String:AnyObject]]()
    var mail: String!
    var message: String!
    var status: Bool!
    var satusString: String!
    var profileImage: String!
    var name: String!
    var codeMessage: Int!
    var userId: Int!
    var gender: String!
    var club: String!
    var memberNumber: Int!
    var height: String!
    var weight: String!
    var age: Int!
    var member_type: String!
    var mainteiment: String!
    var chargesArray = [[String:AnyObject]]()
    var movimientosArray =  [[String:AnyObject]]()
    var noticiasArray = [[String:AnyObject]]()
    var noticaResumen = [String]()
    var tituloArray = [String]()
    var descripcionArray = [String]()
    var pesoArray = [String]()
    var descripcionArray2 = [String]()
    var imagenNoticia = [String]()
    var infoVC = InformacionViewController()
    var nombreConvenio = [String]()
    var clausulasConvenio = [String]()
    var logotipoConvenio = [String]()
    var nombrePase = [String]()
    var finVigencia = [String]()
    var productoPaseInvitado = [String]()
    var cuandoPaseInvitado = [String]()

    static let sharedInstance = APIManager()
    
    func login(authKey: String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params: [String: Any] = [:]
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/login_new/") else {return}
        var  request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.addValue(authKey, forHTTPHeaderField: "auth-key")
        print("Este es el authKey", authKey)
        
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    let result =  try JSON(data: data)
                    self.message = json["message"] as? String
                    self.status = json["status"] as! Bool
                    
                    print("Este es el status", self.status)
                    if self.status == false {
                        print("Valio madres")
                    }else {
                        var data = json ["data"] as! Dictionary<String,AnyObject>
                        for element in data {
                            var profilePic = data["profile_photo"] as? String
                            SavedData.setTheProfilePic(profilePic: profilePic!)
                            let name = data["name"] as! String
                            SavedData.setTheName(theName: name ?? "")
                            self.name = name
                            self.userId = data["user_id"] as! Int
                            self.gender = data["gender"] as? String
                            self.club = data ["club"] as? String
                            self.mail = data ["mail"] as? String
                            self.height = data ["tallest"] as? String
                            print("Aqui estÃ¡ el tallest", self.height)
                            self.weight = data ["weight"] as? String
                            
                           
                            self.age = data["age"] as! Int
                            self.member_type = data ["member_type"] as? String
                            self.mainteiment = data ["mainteiment"] as? String
                            //self.memberNumber = data ["membernumber"] as! Int
                            
                            SavedData.setTheUserId(userId: self.userId)
                            
                            
                        }
                        
                    }
                    print(json )
                    
                    
                    onSuccess(result)
                    
                    
                    
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
        
    }
    func register(email: String, tipo: String, club: String, membresia: String, id: String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["email": email,
                                     "tipo" : tipo,
                                     "club" : club,
                                     "membresia" : membresia,
                                     "id" : id]
        guard let url = URL(string: "http://cloud.sportsworld.com.mx/app/api/v1/usuario/registro")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    let result = try JSON(data: data)
                    let mensaje = json["message"]
                    let code = json["code"] as! Int
                    print("Este es el cÃ³digo", code )
                    
                    self.codeMessage = code
                    self.message = mensaje as! String
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
//////////////////////////////////OBTEN LOS MOVIMIENTOS DEL USUARIO/////////////////////////////////////////
    
    func getPendingCharges(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/movimientos/pendientes/\(SavedData.getTheUserId())") else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    print(json)
                    let result = try JSON(data: data)
                    var status = json["status"] as! Bool
                    var message = json["message"] as? String
                    self.message = message
                    self.status = status
                    let dataReceived = json["data"] as! Dictionary<String,AnyObject>
                    
                    for element in dataReceived {
                        let movimientos = dataReceived["movimientos"]
                        self.movimientosArray = movimientos as! [[String : AnyObject]]
                    }
                    for element in self.movimientosArray {
                        let importeTotal = element["importetotal"]
                        let descripcion = element["descripcion"]
                        self.totalImportes.append(importeTotal as! String)
                        self.totalDescripciones.append(descripcion as! String)
                        print("Aqui estÃ¡ el importeTotal", self.totalImportes)
                    }
                    onSuccess(result)
                    print("Todo ok")
                    
                    
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }
        
    func getClubesForRegister(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        self.allClubes = [Club]()
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/club_list/\(SavedData.getTheLatitude())/\(SavedData.getTheLongitude())/274978/") else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    print(json)
                    let result = try JSON(data: data)
                    let dataReceived = json["data"]
                    self.allClubesInfo = dataReceived as! [[String : AnyObject]]
                    print("Aqui está la data", self.allClubesInfo)
                    
                    ////////////////////OBTEN LOS CLUBES DEL DICCIONARIO P1///////////////////
                    
                    
                    /*
                     Se repiten los elementos de diccionario p1 con el diccionario p2
                     */
                    /////////////////////////////////OBTEN LOS CLUBES DEL DICCIONARIO P2///////////////////////////////////////////////
                    
                    for allClubesP1 in self.allClubesInfo {
                        let clubReceived = allClubesP1["p1"]
                        self.dictionaryP1 = clubReceived as! [[String : AnyObject]]
                        
                    }
                    
                    for clubList1 in self.dictionaryP1 {
                        let list1 = clubList1["list_club"]
                        self.listClubP1 = list1 as! [[String : AnyObject]]
                    }
                    
                    for clubNameP1 in self.listClubP1 {
                        //let nameP2 = clubNameP2["nombre"]
                        //print("Clubes de la p2", nameP2 ?? "")
                        //self.finalList.append(nameP2 as! String)
                        
                        let tempClub : Club = Club()
                        if (clubNameP1["latitud"] as? Double) != nil
                        {
                            tempClub.latitude = clubNameP1["latitud"] as! Double
                        }
                        
                        if (clubNameP1["distance"] as? Double) != nil
                        {
                            tempClub.distance = clubNameP1["distance"] as! Double
                        }
                        
                        if (clubNameP1["agendar"] as? Int) != nil
                        {
                            tempClub.agenda = clubNameP1["agendar"] as! Int
                        }
                        
                        if (clubNameP1["idestado"] as? Int) != nil
                        {
                            tempClub.idEstado = clubNameP1["idestado"] as! Int
                        }
                        
                        if (clubNameP1["direccion"] as? String) != nil
                        {
                            tempClub.address = clubNameP1["direccion"] as! String
                        }
                        
                        
                        if (clubNameP1["horario"] as? String) != nil
                        {
                            tempClub.schedule = clubNameP1["horario"] as! String
                        }
                        
                        if (clubNameP1["idun"] as? Int) != nil
                        {
                            tempClub.clubId = clubNameP1["idun"] as! Int
                        }
                        
                        if (clubNameP1["clave"] as? String) != nil
                        {
                            tempClub.clave = clubNameP1["clave"] as! String
                        }
                        
                        
                        if (clubNameP1["dcount"] as? Int) != nil
                        {
                            tempClub.dCount = clubNameP1["dcount"] as! Int
                        }
                        
                        if (clubNameP1["ruta360"] as? String) != nil
                        {
                            tempClub.schedule = clubNameP1["ruta360"] as! String
                        }
                        
                        if (clubNameP1["nombre"] as? String) != nil
                        {
                            tempClub.name = clubNameP1["nombre"] as! String
                        }
                        
                        if (clubNameP1["rutavideo"] as? String) != nil
                        {
                            tempClub.rutaVideo = clubNameP1["rutavideo"] as! String
                        }
                        
                        if (clubNameP1["preventa"] as? Int) != nil
                        {
                            tempClub.preventa = clubNameP1["preventa"] as! Int
                        }
                        
                        if (clubNameP1["estado"] as? String) != nil
                        {
                            tempClub.estado = clubNameP1["estado"] as! String
                        }
                        
                        if (clubNameP1["longitud"] as? Double) != nil
                        {
                            tempClub.longitude = clubNameP1["longitud"] as! Double
                        }
                        
                        let coordinate₀ = CLLocation(latitude: tempClub.latitude, longitude: tempClub.longitude)
                        let coordinate₁ = CLLocation(latitude: SavedData.getTheLatitude(), longitude: SavedData.getTheLongitude())
                        
                        let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                        tempClub.distance = distanceInMeters
                        tempClub.group = "Favoritos"
                        self.allClubes.append(tempClub)
                        
                        
                    }
                    
                    for allClubesP2 in self.allClubesInfo {
                        let clubReceived = allClubesP2["p2"]
                        self.dictionaryP2 = clubReceived as! [[String : AnyObject]]
                        
                    }
                    
                    for clubList2 in self.dictionaryP2 {
                        let list2 = clubList2["list_club"]
                        self.listClubP2 = list2 as! [[String : AnyObject]]
                    }
                    
                    for clubNameP2 in self.listClubP2 {
                        let nameP2 = clubNameP2["nombre"]
                        //print("Clubes de la p2", nameP2 ?? "")
                        self.finalList.append(nameP2 as! String)
                        
                        let tempClub : Club = Club()
                        if (clubNameP2["latitud"] as? Double) != nil
                        {
                            tempClub.latitude = clubNameP2["latitud"] as! Double
                        }
                        
                        if (clubNameP2["distance"] as? Double) != nil
                        {
                            tempClub.distance = clubNameP2["distance"] as! Double
                        }
                        
                        if (clubNameP2["agendar"] as? Int) != nil
                        {
                            tempClub.agenda = clubNameP2["agendar"] as! Int
                        }
                        
                        if (clubNameP2["idestado"] as? Int) != nil
                        {
                            tempClub.idEstado = clubNameP2["idestado"] as! Int
                        }
                        
                        if (clubNameP2["direccion"] as? String) != nil
                        {
                            tempClub.address = clubNameP2["direccion"] as! String
                        }
                        
                        
                        if (clubNameP2["horario"] as? String) != nil
                        {
                            tempClub.schedule = clubNameP2["horario"] as! String
                        }
                        
                        if (clubNameP2["idun"] as? Int) != nil
                        {
                            tempClub.clubId = clubNameP2["idun"] as! Int
                        }
                        
                        if (clubNameP2["clave"] as? String) != nil
                        {
                            tempClub.clave = clubNameP2["clave"] as! String
                        }
                        
                        
                        if (clubNameP2["dcount"] as? Int) != nil
                        {
                            tempClub.dCount = clubNameP2["dcount"] as! Int
                        }
                        
                        if (clubNameP2["ruta360"] as? String) != nil
                        {
                            tempClub.schedule = clubNameP2["ruta360"] as! String
                        }
                        
                        if (clubNameP2["nombre"] as? String) != nil
                        {
                            tempClub.name = clubNameP2["nombre"] as! String
                        }
                        
                        if (clubNameP2["rutavideo"] as? String) != nil
                        {
                            tempClub.rutaVideo = clubNameP2["rutavideo"] as! String
                        }
                        
                        if (clubNameP2["preventa"] as? Int) != nil
                        {
                            tempClub.preventa = clubNameP2["preventa"] as! Int
                        }
                        
                        if (clubNameP2["estado"] as? String) != nil
                        {
                            tempClub.estado = clubNameP2["estado"] as! String
                        }
                        
                        if (clubNameP2["longitud"] as? Double) != nil
                        {
                            tempClub.longitude = clubNameP2["longitud"] as! Double
                        }
                        
                        let coordinate₀ = CLLocation(latitude: tempClub.latitude, longitude: tempClub.longitude)
                        let coordinate₁ = CLLocation(latitude: SavedData.getTheLatitude(), longitude: SavedData.getTheLongitude())
                        
                        let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                        tempClub.distance = distanceInMeters
                        tempClub.group = "Próximas Aperturas"
                        self.allClubes.append(tempClub)
                        
                        
                    }
                    
                    for id2 in self.listClubP2 {
                        let id2 = id2["idun"]
                        //print("id clubes de la p2", id2 ?? 0)
                        self.finalId.append(id2 as! Int)
                    }
                    
                    //////////////////////////////////OBTEN LOS CLUBES DEL DICCIONARIO P3////////////////////////////////
                    
                    for allClubesP3 in self.allClubesInfo {
                        let clubReceived = allClubesP3["p3"]
                        self.dictionaryP3 = clubReceived as! [[String : AnyObject]]
                        
                    }
                    
                    for clubList3 in self.dictionaryP3 {
                        let list3 = clubList3["list_club"]
                        self.listClubP3 = list3 as! [[String : AnyObject]]
                    }
                    
                    for clubNameP3 in self.listClubP3 {
                        let nameP3 = clubNameP3["nombre"]
                        //print("Clubes de la p3", nameP3 ?? "")
                        self.finalList.append(nameP3 as! String)
                        
                        let tempClub : Club = Club()
                        if (clubNameP3["latitud"] as? Double) != nil
                        {
                            tempClub.latitude = clubNameP3["latitud"] as! Double
                        }
                        
                        if (clubNameP3["distance"] as? Double) != nil
                        {
                            tempClub.distance = clubNameP3["distance"] as! Double
                        }
                        
                        if (clubNameP3["agendar"] as? Int) != nil
                        {
                            tempClub.agenda = clubNameP3["agendar"] as! Int
                        }
                        
                        if (clubNameP3["idestado"] as? Int) != nil
                        {
                            tempClub.idEstado = clubNameP3["idestado"] as! Int
                        }
                        
                        if (clubNameP3["direccion"] as? String) != nil
                        {
                            tempClub.address = clubNameP3["direccion"] as! String
                        }
                        
                        
                        if (clubNameP3["horario"] as? String) != nil
                        {
                            tempClub.schedule = clubNameP3["horario"] as! String
                        }
                        
                        if (clubNameP3["idun"] as? Int) != nil
                        {
                            tempClub.clubId = clubNameP3["idun"] as! Int
                        }
                        
                        if (clubNameP3["clave"] as? String) != nil
                        {
                            tempClub.clave = clubNameP3["clave"] as! String
                        }
                        
                        
                        if (clubNameP3["dcount"] as? Int) != nil
                        {
                            tempClub.dCount = clubNameP3["dcount"] as! Int
                        }
                        
                        if (clubNameP3["ruta360"] as? String) != nil
                        {
                            tempClub.schedule = clubNameP3["ruta360"] as! String
                        }
                        
                        if (clubNameP3["nombre"] as? String) != nil
                        {
                            tempClub.name = clubNameP3["nombre"] as! String
                        }
                        
                        if (clubNameP3["rutavideo"] as? String) != nil
                        {
                            tempClub.rutaVideo = clubNameP3["rutavideo"] as! String
                        }
                        
                        if (clubNameP3["preventa"] as? Int) != nil
                        {
                            tempClub.preventa = clubNameP3["preventa"] as! Int
                        }
                        
                        if (clubNameP3["estado"] as? String) != nil
                        {
                            tempClub.estado = clubNameP3["estado"] as! String
                        }
                        
                        if (clubNameP3["longitud"] as? Double) != nil
                        {
                            tempClub.longitude = clubNameP3["longitud"] as! Double
                        }
                        
                        let coordinate₀ = CLLocation(latitude: tempClub.latitude, longitude: tempClub.longitude)
                        let coordinate₁ = CLLocation(latitude: SavedData.getTheLatitude(), longitude: SavedData.getTheLongitude())
                        
                        let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                        tempClub.distance = distanceInMeters
                        tempClub.group = "Cerca de mi"
                        self.allClubes.append(tempClub)
                    }
                    
                    for id3 in self.listClubP3{
                        let id3 = id3["idun"]
                        //print("id clubes de la p3", id3 ?? 0)
                        self.finalId.append(id3 as! Int)
                    }
                    
                    /////7///////////////////////////////OBTEN LOS CLUBES DEL DICCIONARIO P4///////////////////////////////
                    
                    for allClubesP4 in self.allClubesInfo {
                        let clubReceived = allClubesP4["p4"]
                        self.dictionaryP4 = clubReceived as! [[String : AnyObject]]
                        
                    }
                    
                    for clubList4 in self.dictionaryP4 {
                        let list4 = clubList4["list_club"]
                        self.listClubP4 = list4 as! [[String : AnyObject]]
                    }
                    
                    for clubNameP4 in self.listClubP4 {
                        let nameP4 = clubNameP4["nombre"]
                        self.finalList.append(nameP4 as! String)
                        
                        let tempClub : Club = Club()
                        if (clubNameP4["latitud"] as? Double) != nil
                        {
                            tempClub.latitude = clubNameP4["latitud"] as! Double
                        }
                        
                        if (clubNameP4["distance"] as? Double) != nil
                        {
                            tempClub.distance = clubNameP4["distance"] as! Double
                        }
                        
                        if (clubNameP4["agendar"] as? Int) != nil
                        {
                            tempClub.agenda = clubNameP4["agendar"] as! Int
                        }
                        
                        if (clubNameP4["idestado"] as? Int) != nil
                        {
                            tempClub.idEstado = clubNameP4["idestado"] as! Int
                        }
                        
                        if (clubNameP4["direccion"] as? String) != nil
                        {
                            tempClub.address = clubNameP4["direccion"] as! String
                        }
                        
                        
                        if (clubNameP4["horario"] as? String) != nil
                        {
                            tempClub.schedule = clubNameP4["horario"] as! String
                        }
                        
                        if (clubNameP4["idun"] as? Int) != nil
                        {
                            tempClub.clubId = clubNameP4["idun"] as! Int
                        }
                        
                        if (clubNameP4["clave"] as? String) != nil
                        {
                            tempClub.clave = clubNameP4["clave"] as! String
                        }
                        
                        
                        if (clubNameP4["dcount"] as? Int) != nil
                        {
                            tempClub.dCount = clubNameP4["dcount"] as! Int
                        }
                        
                        if (clubNameP4["ruta360"] as? String) != nil
                        {
                            tempClub.schedule = clubNameP4["ruta360"] as! String
                        }
                        
                        if (clubNameP4["nombre"] as? String) != nil
                        {
                            tempClub.name = clubNameP4["nombre"] as! String
                        }
                        
                        if (clubNameP4["rutavideo"] as? String) != nil
                        {
                            tempClub.rutaVideo = clubNameP4["rutavideo"] as! String
                        }
                        
                        if (clubNameP4["preventa"] as? Int) != nil
                        {
                            tempClub.preventa = clubNameP4["preventa"] as! Int
                        }
                        
                        if (clubNameP4["estado"] as? String) != nil
                        {
                            tempClub.estado = clubNameP4["estado"] as! String
                        }
                        
                        if (clubNameP4["longitud"] as? Double) != nil
                        {
                            tempClub.longitude = clubNameP4["longitud"] as! Double
                        }
                        
                        let coordinate₀ = CLLocation(latitude: tempClub.latitude, longitude: tempClub.longitude)
                        let coordinate₁ = CLLocation(latitude: SavedData.getTheLatitude(), longitude: SavedData.getTheLongitude())
                        
                        let distanceInMeters = coordinate₀.distance(from: coordinate₁)
                        tempClub.distance = distanceInMeters
                        tempClub.group = "Cerca de mi"
                        self.allClubes.append(tempClub)
                    }
                    
                    
                    for id4 in self.listClubP3{
                        let id4 = id4["idun"]
                        //print("id clubes de la p4", id4 ?? 0)
                        self.finalId.append(id4 as! Int)
                    }
                    
                    print("Lista ids",self.finalId.removeDuplicates())
                    print("Lista clubes",self.finalList.removeDuplicates())
                    //self.allClubes = self.allClubes.removingDuplicates(byKey: {$0.name})
                    onSuccess(result)
                    print("Todo ok")
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }
    func getClasessByClub(date:Date,idClub:Int, onSuccess: @escaping([ClassSW]) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let calendar = Calendar.current
        
        
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy/MM/dd"
        
        let result = formatter.string(from: date)
        
        guard let url = URL(string: "https://prepago.sportsworld.com.mx/api/v1/app/class/list/reservation/\(idClub)/\(result)/\(SavedData.getTheUserId())/") else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    let dataReceived = json["data"]
                    
                    print(dataReceived ?? "")
                    var tempArrayClasses : [ClassSW] = [ClassSW]()
                    for clubData in dataReceived as! [[String : AnyObject]]{
                        let tempClass : ClassSW = ClassSW()
                        if (clubData["idinstalacionactividadprogramada"] as? Int) != nil
                        {
                            tempClass.idinstalacionactividadprogramada = clubData["idinstalacionactividadprogramada"] as! Int
                        }
                        
                        if (clubData["club"] as? String) != nil
                        {
                            tempClass.club = clubData["club"] as! String
                        }
                        
                        if (clubData["salon"] as? String) != nil
                        {
                            tempClass.salon = clubData["salon"] as! String
                        }
                        
                        if (clubData["idsalon"] as? Int) != nil
                        {
                            tempClass.idsalon = clubData["idsalon"] as! Int
                        }
                        
                        if (clubData["clase"] as? String) != nil
                        {
                            tempClass.clase = clubData["clase"] as! String
                        }
                        
                        if (clubData["idclase"] as? Int) != nil
                        {
                            tempClass.idclase = clubData["idclase"] as! Int
                        }
                        
                        if (clubData["instructor"] as? String) != nil
                        {
                            tempClass.instructor = clubData["instructor"] as! String
                        }
                        
                        if (clubData["inicio"] as? String) != nil
                        {
                            tempClass.inicio = clubData["inicio"] as! String
                        }
                        
                        if (clubData["fin"] as? String) != nil
                        {
                            tempClass.fin = clubData["fin"] as! String
                        }
                        
                        if (clubData["iniciovigencia"] as? String) != nil
                        {
                            tempClass.iniciovigencia = clubData["iniciovigencia"] as! String
                        }
                        
                        if (clubData["finvigencia"] as? String) != nil
                        {
                            tempClass.finvigencia = clubData["finvigencia"] as! String
                        }
                        
                        if (clubData["capacidadideal"] as? Int) != nil
                        {
                            tempClass.capacidadideal = clubData["capacidadideal"] as! Int
                        }
                        
                        if (clubData["capacidadmaxima"] as? Int) != nil
                        {
                            tempClass.capacidadmaxima = clubData["capacidadmaxima"] as! Int
                        }
                        
                        if (clubData["capacidadregistrada"] as? Int) != nil
                        {
                            tempClass.capacidadregistrada = clubData["capacidadregistrada"] as! Int
                        }
                        
                        if (clubData["reservacion"] as? Int) != nil
                        {
                            tempClass.reservacion = clubData["reservacion"] as! Int
                        }
                        
                        if (clubData["confirmados"] as? Int) != nil
                        {
                            tempClass.confirmados = clubData["confirmados"] as! Int
                        }
                        
                        if (clubData["agendar"] as? Int) != nil
                        {
                            tempClass.agendar = clubData["agendar"] as! Int
                        }
                        
                        if (clubData["demand"] as? Int) != nil
                        {
                            tempClass.demand = clubData["demand"] as! Int
                        }
                        
                        if (clubData["inscrito"] as? Bool) != nil
                        {
                            tempClass.inscrito = clubData["inscrito"] as! Bool
                        }
                        
                        tempArrayClasses.append(tempClass)
                    }
                    
                    onSuccess(tempArrayClasses)
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }
    
    func makeReservation(params : [String:Any], onSuccess: @escaping(GenericResponse) -> Void, onFailure: @escaping(Error) -> Void) {
        

        guard let url = URL(string: "https://prepago.sportsworld.com.mx/api/v1/app/class/reservation")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let j = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    let result : GenericResponse = GenericResponse()
                    if (j["message"] as? String) != nil
                    {
                        result.message = j["message"] as! String
                    }
                    
                    if (j["status"] as? Bool) != nil
                    {
                        result.status = j["status"] as! Bool
                    }
                    
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
    func getClassDescription(idClass : Int, onSuccess: @escaping(DescriptionResponse) -> Void, onFailure: @escaping(Error) -> Void) {
        
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/descipcion_clase/\(idClass)/")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        /*
         guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/news") else {return}
         var  request = URLRequest(url: url)
         request.httpMethod = "GET"
         let session = URLSession.shared
         let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in*/
        
        
        
        let session = URLSession.shared
        //request.httpBody = httpBody
        
        //let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let j = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    let result : DescriptionResponse = DescriptionResponse()
                    if (j["message"] as? String) != nil
                    {
                        result.message = j["message"] as! String
                    }
                    
                    if (j["status"] as? Bool) != nil
                    {
                        result.status = j["status"] as! Bool
                        if(result.status){
                            let dataReceived = j["data"] as! [Dictionary<String,AnyObject>]
                            if(dataReceived.count > 0){
                                if (dataReceived[0]["descripcionHTML"] as? String) != nil
                                {
                                    result.data.descripcionHTML = dataReceived[0]["descripcionHTML"] as! String
                                }
                            }
                        }
                    }
                    
                    
                    
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
    func cancelReservation(params : [String:AnyObject], onSuccess: @escaping(GenericResponse) -> Void, onFailure: @escaping(Error) -> Void) {
        
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/class/cancelar_reservacion/")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let j = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    //let result = try JSON(data: data)
                    let result : GenericResponse = GenericResponse()
                    if (j["message"] as? String) != nil
                    {
                        result.message = j["message"] as! String
                    }
                    
                    if (j["status"] as? Bool) != nil
                    {
                        result.status = j["status"] as! Bool
                    }
                    
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
    func getClubDetail(onSuccess: @escaping(ClubResponse) -> Void, onFailure: @escaping(Error) -> Void) {
        
        
        guard let url = URL(string:"https://app.sportsworld.com.mx/api/v2/club/details/\( APIManager.sharedInstance.selectedClub.clubId)/0/")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        /*
 guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/news") else {return}
 var  request = URLRequest(url: url)
 request.httpMethod = "GET"
 let session = URLSession.shared
 let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in*/
        
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let j = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    //let result = try JSON(data: data)
                    let result : ClubResponse = ClubResponse()
                    if (j["message"] as? String) != nil
                    {
                        result.message = j["message"] as! String
                    }
                    
                    if (j["status"] as? Bool) != nil
                    {
                        result.status = j["status"] as! Bool
                    }
                    let dataReceived = j["data"] as! Dictionary<String,AnyObject>
                    
                    if (dataReceived["latitud"] as? Double) != nil
                    {
                        result.data.latitud = dataReceived["latitud"] as! Double
                    }
                    
                    if (dataReceived["longitud"] as? Double) != nil
                    {
                        result.data.longitud = dataReceived["longitud"] as! Double
                    }
                    
                    if (dataReceived["agendar"] as? Int) != nil
                    {
                        result.data.agendar = dataReceived["agendar"] as! Int
                    }
                    
                    if (dataReceived["favorito"] as? Bool) != nil
                    {
                        result.data.favorito = dataReceived["favorito"] as! Bool
                    }
                    
                    if (dataReceived["direccion"] as? String) != nil
                    {
                        result.data.direccion = dataReceived["direccion"] as! String
                    }
                    
                    if (dataReceived["horario"] as? String) != nil
                    {
                        result.data.horario = dataReceived["horario"] as! String
                    }
                    
                    if (dataReceived["idun"] as? Int) != nil
                    {
                        result.data.idun = dataReceived["idun"] as! Int
                    }
                    
                    if (dataReceived["imagenes_club"] as? [String]) != nil
                    {
                        result.data.imagenes_club = dataReceived["imagenes_club"] as! [String]
                    }
                    
                    if (dataReceived["ruta360"] as? String) != nil
                    {
                        result.data.ruta360 = dataReceived["ruta360"] as! String
                    }
                    
                    if (dataReceived["nombre"] as? String) != nil
                    {
                        result.data.nombre = dataReceived["nombre"] as! String
                    }
                    
                    if (dataReceived["rutavideo"] as? String) != nil
                    {
                        result.data.rutavideo = dataReceived["rutavideo"] as! String
                    }
                    
                    if (dataReceived["preventa"] as? Int) != nil
                    {
                        result.data.preventa = dataReceived["preventa"] as! Int
                    }
                    
                    if (dataReceived["telefono"] as? String) != nil
                    {
                        result.data.telefono = dataReceived["telefono"] as! String
                    }
                    
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
/////////////////////////////CONEKTA REQUEST///////////////////////////////
    func conekta(token_id: String, person_id: Int, name: String,
                 unit_price:Int, quantity: Int, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["token_id": token_id,
                                     "person_id" : person_id,
                                     "line_items" : ["name" : name,
                                                     "unit_price": unit_price,
                                                     "quantity": quantity]
                                     ]
        guard let url = URL(string: "http://cloud.sportsworld.com.mx/api/v1/app/conekta")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    let result = try JSON(data: data)
                    let mensaje = json["message"]
                    let code = json["code"]
                    
                    print("Este es el mensaje de conekta", mensaje ?? "")
                    self.message = mensaje as! String
                    self.codeMessage = code as! Int
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
    //////////////////////////////OBTEN PUNTOS EARN IT//////////////////////////////////////////////////
    func earnIt(userId: Int, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["id": userId]
        guard let url = URL(string: "http://manticore.4p.com.mx/manticore/beneficiarioRewards/puntos")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("tokenCliente", forHTTPHeaderField: "5p0r75w0rlD09632")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    let result = try JSON(data: data)
                    let success = json["success"]
                    let mensaje = json["data"]
                    self.status = success as! Bool
                    self.message = mensaje as! String
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
///////////////////////////////////CONFIGURACIÃ“N////////////////////////////////////////////
   
    func profileUpdate(height: Double, weight: Double, age: Int, img:String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["height": height,
                                     "weight": weight,
                                     "age": age,
                                     "user_id": SavedData.getTheUserId(),
                                     "dob": "1983-05-03",
                                     "memunic_id": 0,
                                     "img": img]
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/profile/update/v2/")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    let result = try JSON(data: data)
                    let success = json["status"]
                    let mensaje = json["message"]
                    self.status = success as! Bool
                    self.message = mensaje as! String
                    
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
                    
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
///////////////////////////RECUPERAR CONTRASEÃ‘A////////////////////////////
    
    func recoverPassword(email:String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["email": email]
        guard let url = URL(string: "http://cloud.sportsworld.com.mx/app/api/v1/usuario/pass/recuperar")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
      
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    let result = try JSON(data: data)
                    let mensaje = json["message"] as? String
                    let code = json ["code"]
                    self.codeMessage = code as! Int
                    self.message = mensaje
                    
                    print("Todo ok")
                    print(json )
                    onSuccess(result)
                    
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
//////////////////////////////CONSULTA LAS NOTICIAS///////////////////////
    func getNews(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/news") else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    print(json)
                    let result = try JSON(data: data)
                    var status = json["status"] as! Bool
                    self.status = status
                    let dataReceived = json["data"] as! Dictionary<String,AnyObject>
                    
                    for element in dataReceived {
                        let noticias = dataReceived["by_permanent"]
                        self.noticiasArray = noticias as! [[String : AnyObject]]
                    }
                    for element in self.noticiasArray {
                        let resumen = element["resumen"]
                        let titulo = element["titulo"]
                        let descripcion = element["descripcion"]
                        let descripcion2 = element["categorianoticia"]
                        let imagenNoticia = element["rutaimagen"]
                        self.imagenNoticia.append(imagenNoticia as! String)
                        self.descripcionArray2.append(descripcion2 as! String)
                        self.descripcionArray.append(descripcion as! String)
                        self.tituloArray.append(titulo as! String)
                        self.noticaResumen.append(resumen as! String)
                    }
                    onSuccess(result)
                    print("Todo ok")
                    
                    
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }

/////////////////////////////////////////CONSULTA HISTORIAL INBODY//////////////////////////////////////////////////
    func getInbodyHistorial(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        guard let url = URL(string: "http://sandbox.sportsworld.com.mx/piso/api/v1/inBody/1070137") else {return}
        ////++++++Se hardcodea el idPersona para que regrese la info hay que cambiarlo
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    print(json)
                    let result = try JSON(data: data)
                    var status = json["status"] as! String
                    var  mensaje = json["message"] as! String
                    self.satusString = status
                    self.message = mensaje
                    let dataReceived = json["data"] as!  [[String : AnyObject]]
                    
                    for element in dataReceived {
                      //let peso = dataReceived ["peso"]
                    }
                    onSuccess(result)
                    print("Todo ok")
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }
/////////////////////////////////SEND PASES DE INVITADO///////////////////////////////////
    
        func enviarPasesInvitado(to: String, nameto: String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
            let params : [String:Any] = ["idPersona": SavedData.getTheUserId(),
                                         "to": to,
                                         "nameto": nameto,
                                         "subject": "Pase de Invitado"]
            
        guard let url = URL(string: "http://sandbox.sportsworld.com.mx/app/api/v1/pases/mailPaseInvitado")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    let result = try JSON(data: data)
                    let mensaje = json["message"]
                    let code = json["code"] as! Int
                    self.codeMessage = code
                    self.message = mensaje as! String
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
        
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
//////////////////////////// GET CONVENIOS//////////////////////////////////////////
    
    func getConvenios(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Bool) -> Void) {
        guard let url = URL(string: "http://sandbox.sportsworld.com.mx/beneficios/api/v1/readBeneficio/") else {return}
        ////++++++Se hardcodea el idPersona para que regrese la info hay que cambiarlo
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as!  Dictionary<String,AnyObject>
                    print(json)
                    let result = try JSON(data: data)
                    
                    let beneficios = json["beneficios"] as! [[String:AnyObject]]
                    print("Beneficios aqui estan", beneficios)
                    for element in beneficios {
                        let beneficioName = element["nombre"] as! String
                        let clausulasBeneficio = element["clausulas"] as! String
                        let logotipo = element["logotipo"] as! String
                        self.clausulasConvenio.append(clausulasBeneficio)
                        self.logotipoConvenio.append(logotipo)
                        self.nombreConvenio.append(beneficioName)
                    }
                 
                    
                    onSuccess(result)
                    print("Todo ok")
                } catch {
                    print(error)
                    print("OcurriÃ³ un error")
                }
            }
            if(error != nil){
                onFailure(error! as! Bool)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }

///////////////////////////////////CONECTA PASES POR MEMBRESIA////////////////////////////////
    func getPasesDeInvitado(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Bool) -> Void) {
        guard let url = URL(string: "http://sandbox.sportsworld.com.mx/app/api/v1/pases/readPasesPorMembresia/\(SavedData.getTheUserId())") /*cambiar el id de usuario de SavedData*/else {return}
        ////++++++Se hardcodea el idPersona para que regrese la info hay que cambiarlo
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    print(json)
                    let result = try JSON(data: data)
                    var pases = json["pases"] as! [[String:AnyObject]]
                    var asignados = json["asignados"] as! [[String:AnyObject]]
                    for element in pases {
                        let nombre = element["nombre"] as! String
                        let finVigencia = element["finVigencia"] as! String
                        self.finVigencia.append(finVigencia)
                        self.nombrePase.append(nombre)
                        
                    }
                    for element in asignados {
                        let producto = element["producto"] as! String
                        let cuando = element["cuando"] as! String
                        self.productoPaseInvitado.append(producto)
                        self.cuandoPaseInvitado.append(cuando)
                        
                    }
                    onSuccess(result)
                } catch {
                    print(error)
                }
            }
            if(error != nil){
                onFailure(error! as! Bool)
            } else{
            }
        })
        task.resume()
    }
}




