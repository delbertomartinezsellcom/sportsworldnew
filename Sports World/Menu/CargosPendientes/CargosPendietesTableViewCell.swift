//
//  CargosPendietesTableViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 5/16/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class CargosPendietesTableViewCell: UITableViewCell {

    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var backgrdndView: UIView!
    @IBOutlet weak var amountCharge: UILabel!
    @IBOutlet weak var descriptionCharge: UILabel!
    @IBOutlet weak var titleCharge: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgrdndView.layer.cornerRadius = 10
        checkImage.layer.cornerRadius = 20 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
