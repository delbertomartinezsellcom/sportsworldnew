//
//  MembresiaViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 5/10/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class MembresiaViewController: UIViewController {
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var user = APIManager.sharedInstance.name
    var profileImageURL = SavedData.getTheProfilePic()
    var club = APIManager.sharedInstance.club
    var memberNumber = APIManager.sharedInstance.memberNumber
    /////////////////OUTLETS DE LA TARJETA DE FRENTE////////////
    
    @IBOutlet weak var card_front: UIImageView!
    @IBOutlet weak var viewToTouch: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var baseClub: UILabel!
    @IBOutlet weak var memberShipNumber: UILabel!
    @IBOutlet weak var bienvenidoLabel: UILabel!
    @IBOutlet weak var esfuerzateLabel: UIImageView!
    
    ///////////////////OUTLETS BOTONES///////////////////////
    @IBOutlet weak var pasesDeInvitadoButton: UIButton!
    @IBOutlet weak var beneficiosButton: UIButton!
    @IBOutlet weak var segurosButton: UIButton!
    
    
    var  isOpen = false
    
    //////////////////////OUTLESTS DE LA TARJETA DETRÁS///////////////
    @IBOutlet weak var qrImage: UIImageView!
    
    /////////OUTLETS DE LOS BOTONES///////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activity.isHidden = true
        /////////VALIDACIONES SI LOS CAMPOS ESTÁN VACIOS//////
        if profileImageURL == nil {
            userImage.image = #imageLiteral(resourceName: "back_button")
        } else {
            let url = URL(string: profileImageURL)
            let data = try? Data(contentsOf: url!)
            userImage.image = UIImage(data: data!)
        }
        if userName == nil {
            userName.text = ""
        } else {
           userName.text = user
        }
        if club == nil {
        baseClub.text = ""
        } else {
            baseClub.text = club
        }
        if memberNumber == nil {
            memberShipNumber.text = ""
        } else {
            let memberToString: String! = String(describing: memberNumber ?? 0)
            print("Aqui está el memberToString", memberToString)
            memberShipNumber.text! = memberToString
        }
////////////////////////CONFIGURA LAS LINEAS DE ARRIBA Y ABAJO DE LOS BOTONES////////
        pasesDeInvitadoButton.addTopBorderWithColor(pasesDeInvitadoButton, color: UIColor.white, width: 0.4)
        beneficiosButton.addTopBorderWithColor(beneficiosButton, color: UIColor.white, width: 0.4)
        
      
        
        beneficiosButton.addBottomBorderWithColor(beneficiosButton, color: UIColor.white, width: 0.4)
        segurosButton.addBottomBorderWithColor(segurosButton, color: UIColor.white, width: 0.4)
        
        
        bienvenidoLabel.isHidden = true
        esfuerzateLabel.isHidden = true
       // qrImage.isHidden = true
      //  isOpen = true
       // let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
      //  self.viewToTouch.addGestureRecognizer(gesture)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
   /* @objc func checkAction(sender : UITapGestureRecognizer) {
        if isOpen{
            isOpen = false
            let image = UIImage(named: "credencial_front")
            card_front.image = image
            memberShipNumber.isHidden = false
            userName.isHidden = false
            userImage.isHidden = false
            baseClub.isHidden = false
            qrImage.isHidden = true
            bienvenidoLabel.isHidden = true
            esfuerzateLabel.isHidden = true
            pasesDeInvitadoButton.isHidden = false
            beneficiosButton.isHidden = false
            segurosButton.isHidden = false
            
            UIView.transition(with: card_front, duration: 0.3, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        } else {
            isOpen = true
            let image = UIImage(named: "credencial_back")
            card_front.image = image
            userName.isHidden = true
            userImage.isHidden = true
            baseClub.isHidden = true
            qrImage.isHidden = false
            memberShipNumber.isHidden = true
            bienvenidoLabel.isHidden = false
            esfuerzateLabel.isHidden = false
            pasesDeInvitadoButton.isHidden = true
            beneficiosButton.isHidden = true
            segurosButton.isHidden = true
            UIView.transition(with: card_front, duration: 0.3, options: .transitionFlipFromRight, animations: nil, completion: nil)
        }
    }*/
    
    //MARK:- BOTONES ACCIONES
    
    @IBAction func clickPasesDeInvitadoButton(_ sender: Any) {
       self.getPasesInvitado()
    }
    
    @IBAction func clickSegurosButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "SegurosViewController") as! SegurosViewController
        self.navigationController!.pushViewController(VC1, animated: true)
    }
    
    @IBAction func clickBeneficiosButton(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            self.getConvenios()
        }
        
    }
    
    func getConvenios() {
       APIManager.sharedInstance.logotipoConvenio = []
       APIManager.sharedInstance.nombreConvenio = []
       APIManager.sharedInstance.clausulasConvenio = []
        
        activity.isHidden = false
        activity.startAnimating()
        self.view.isUserInteractionEnabled = false
        APIManager.sharedInstance.getConvenios(onSuccess: { json in
            DispatchQueue.main.async {
                self.activity.isHidden = true
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                    print("Success Request Getting Convenios")
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "BeneficiosViewController") as! BeneficiosViewController
                self.navigationController!.pushViewController(VC1, animated: true)
                
            }
            
        }, onFailure: { error in
            self.activity.isHidden = true
            self.activity.stopAnimating()
            self.view.isUserInteractionEnabled = true
        })
    }
    func getPasesInvitado() {
        APIManager.sharedInstance.productoPaseInvitado = []
        APIManager.sharedInstance.cuandoPaseInvitado = []
        APIManager.sharedInstance.nombrePase = []
        APIManager.sharedInstance.finVigencia = []
        activity.isHidden = false
        activity.startAnimating()
        self.view.isUserInteractionEnabled = false
        APIManager.sharedInstance.getPasesDeInvitado(onSuccess: { json in
            DispatchQueue.main.async {
                self.activity.isHidden = true
                self.activity.stopAnimating()
                self.view.isUserInteractionEnabled = true
                print("Success Request Getting Convenios")
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "PasesDeInvitadoViewController") as! PasesDeInvitadoViewController
                self.navigationController!.pushViewController(VC1, animated: true)
                
            }
            
        }, onFailure: { error in
            self.activity.isHidden = true
            self.activity.stopAnimating()
            self.view.isUserInteractionEnabled = true
        })
    }
    
    
}
