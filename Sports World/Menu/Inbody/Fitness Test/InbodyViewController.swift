//
//  InbodyViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/6/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class InbodyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   var pesoArray = APIManager.sharedInstance.pesoArray
    
    /////EN ESTE CONTROLADOR FALTA CREAR EL ARRAY SIN QUE SE REPITAN LOS ELELEMENTOS 
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    private let cell = "InbodyTableViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        activity.isHidden = true

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
            
        }
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            DispatchQueue.main.async {
                if Reachability.isConnectedToNetwork() {
                    self.getInbodyHistorial()
                    
                }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
         pesoArray.removeAll()
    }
            
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pesoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! InbodyTableViewCell
        
        cel.pesoLabel.text = pesoArray[indexPath.row]
        
        return cel
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 687
    }
   
 
    
    func getInbodyHistorial(){
        activity.isHidden = false
        activity.startAnimating()
        self.view.isUserInteractionEnabled = false
        APIManager.sharedInstance.getInbodyHistorial(onSuccess: { json in
            DispatchQueue.main.async {
                if APIManager.sharedInstance.satusString == "ok" {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    print("GetInbodyHistorial success")
                } else {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    Alert.ShowAlert(title: "¡ATENCIÓN!", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
                }
            }
            
        }, onFailure: { error in
            
        })
    }

}
