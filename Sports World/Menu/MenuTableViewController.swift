
//
//  MenuTableViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/1/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    @IBOutlet weak var membresiaButton: UIButton!
    @IBOutlet weak var earnitButton: UIButton!
    @IBOutlet weak var inbodyButton: UIButton!
    @IBOutlet weak var tiendaButton: UIButton!
    @IBOutlet weak var cargosButtton: UIButton!
    @IBOutlet weak var facturasButton: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       activity.isHidden = true 
        facturasButton.isHidden = true
        facturasButton.isEnabled = false
        
        cargosButtton.isHidden = true
        cargosButtton.isEnabled = false
        
        tiendaButton.isHidden = true
        tiendaButton.isEnabled = false
        
        inbodyButton.isHidden = true
        inbodyButton.isEnabled = false
        
        
        //////////////////////PERSONALIZA LOS BOTONES////////////////
        membresiaButton.addTopBorderWithColor(membresiaButton, color: UIColor.white, width: 0.5)
        earnitButton.addTopBorderWithColor(earnitButton, color: UIColor.white, width: 0.5)
        earnitButton.addBottomBorderWithColor(earnitButton, color: UIColor.white, width: 0.5)
        inbodyButton.addTopBorderWithColor(inbodyButton, color: UIColor.white, width: 0.5)
        tiendaButton.addTopBorderWithColor(tiendaButton, color: UIColor.white, width: 0.5)
         tiendaButton.addBottomBorderWithColor(tiendaButton, color: UIColor.white, width: 0.5)
       cargosButtton.addBottomBorderWithColor(cargosButtton, color: UIColor.white, width: 0.5)
        
        facturasButton.addBottomBorderWithColor(facturasButton, color: UIColor.white, width: 0.5)
        
        
        
    }

  

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
//////////////////CONECTA LAS ACCIONES DE LOS BOTONES//////////////////
    @IBAction func clickMembresiaButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MembresiaViewController") as! MembresiaViewController
        self.navigationController!.pushViewController(VC1, animated: true)
    }
    @IBAction func clickEarnitButton(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            earnIt()
        } else {
            Alert.ShowAlert(title: "¡ATENCIÓN!", message: "No cuentas con internet, verifica tu conexión o intenta mas tarde.", titleForTheAction: "ACEPTAR", in: self)
        }
    }
    @IBAction func clickInbodyButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "RutinasViewController") as! RutinasViewController
        self.navigationController!.pushViewController(VC1, animated: true)
        
    }
    
    @IBAction func clickTiendaButton(_ sender: Any) {
        
    }
    @IBAction func clickCargosButton(_ sender: Any) {
       self.getThePendingCharges()
    }
    @IBAction func clickFacturasButton(_ sender: Any) {
        
    }
    
    //////////////////LLAMA A LOS CARGOS/////////////
    func getThePendingCharges(){
        self.view.isUserInteractionEnabled = false
        activity.startAnimating()
        activity.isHidden = false
        APIManager.sharedInstance.getPendingCharges(onSuccess: { json in
            DispatchQueue.main.async {
                if APIManager.sharedInstance.status == true {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "CargosPendietesViewController") as! CargosPendietesViewController
                    self.navigationController!.pushViewController(VC1, animated: true)
                } else {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    Alert.ShowAlert(title: "ATENCIÓN", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                }
                
            }
            
            
        }, onFailure: { error in
            self.activity.isHidden = true
            self.activity.stopAnimating()
        })
        
    }

    ////////////////LLAMA A REQUEST////////////////////////////////////
    func earnIt() {
        activity.isHidden = false
        activity.startAnimating()
        view.isUserInteractionEnabled = false
        APIManager.sharedInstance.earnIt(userId: SavedData.getTheUserId(), onSuccess: { json in
            
            DispatchQueue.main .async {
                if APIManager.sharedInstance.status == true {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    Alert.ShowAlert(title: "ATENCIÓN", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
                } else {
                    self.activity.isHidden = true
                    self.activity.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    Alert.ShowAlert(title: "ATENCIÓN", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
                }
            }
        }, onFailure: { error in
            self.activity.isHidden = true
            self.activity.stopAnimating()
            self.view.isUserInteractionEnabled = true
            Alert.ShowAlert(title: "ATENCIÓN", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
        })
    }
    
}
