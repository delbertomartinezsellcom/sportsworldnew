//
//  ConfiguracionViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 5/22/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import MobileCoreServices

class ConfiguracionViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var newPick: Bool? 
    var finalImage = String()
     var profileImageURL = SavedData.getTheProfilePic()
    //////////VARIABLES////////////////////
    var currentTextField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        if profileImageURL == nil {
            profileImage.image = #imageLiteral(resourceName: "back_button")
        } else {
            let url = URL(string: profileImageURL)
            let data = try? Data(contentsOf: url!)
            profileImage.image = UIImage(data: data!)
        }
        profileImage.frame = CGRect(x: self.view.frame.width / 2 - 35 , y: 80, width: 70, height: 70)
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        profileImage.contentMode = .scaleAspectFill
        
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "power_button"), style: .done, target: self, action: #selector(ConfiguracionViewController.logout))
        rightBarButtonItem.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        activity.isHidden = true
      
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ConfiguracionViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    
    @IBAction func clickConfig(_ sender: Any) {
        logout()
    }
    @objc func logout() {
        activity.isHidden = false
        activity.startAnimating()
        self.view.isUserInteractionEnabled = false
        SavedData.setTheName(theName: "")
        SavedData.setTheUserId(userId: 0)
        SavedData.setTheLatitude(double: 0.0)
        SavedData.setTheLongitude(double: 0.0)
        self.present(SplashViewController(), animated: true, completion: nil)
        activity.isHidden = true
        activity.stopAnimating()
        self.view.isUserInteractionEnabled = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    //DESAPARECE EL TECLADO TOCANDO LA VISTA.
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func updateUserInfo() {
        activity.isHidden = false
        activity.startAnimating()
        view.isUserInteractionEnabled = false
        APIManager.sharedInstance.profileUpdate(height: height!, weight: weight!, age: age!, img: finalImage ,onSuccess: { json in
            DispatchQueue.main.async {
                if APIManager.sharedInstance.status == true {
                    print("success updateprofile")
                    self.activity.stopAnimating()
                    self.activity.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    Alert.ShowAlert(title: "¡ATENCIÓN!", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)

                } else if APIManager.sharedInstance.status == false {
                    self.activity.stopAnimating()
                    self.activity.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    Alert.ShowAlert(title: "¡ATENCIÓN!", message: APIManager.sharedInstance.message, titleForTheAction: "ACEPTAR", in: self)
                }
            }
            
        }, onFailure: { error in
            self.activity.stopAnimating()
            self.activity.isHidden = true
            self.view.isUserInteractionEnabled = true

        })
    }
    @IBAction func clickActualizarButton(_ sender: Any) {
        
        updateUserInfo()
    }
    
    @IBAction func selectPhoto(_ sender: Any) {
        let myAlert = UIAlertController(title: "Seleccionar imagen.", message: "", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Cámara", style: UIAlertActionStyle.default) {
            UIAlertAction in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    var imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera;
                    imagePicker.allowsEditing = false
                    self.present(imagePicker, animated: true, completion: nil)
                }
            }
        }
        let cameraRoll = UIAlertAction(title: "Álbum de fotos", style: UIAlertActionStyle.default) {
            UIAlertAction in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                var imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        myAlert.addAction(cameraAction)
        myAlert.addAction(cameraRoll)
        self.present(myAlert, animated: true , completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        profileImage.image = image
        let imageToConvert: UIImage = profileImage.image!
        if let imageData = imageToConvert.jpeg(.lowest) {
            let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
            finalImage = strBase64
            
        }
        
        dismiss(animated:true, completion: nil)
    }
   

}
