//
//  ConfiguracionTableViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 5/25/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
var height: Double?
var weight : Double?
var age: Int?
class ConfiguracionTableViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var membershipTxtField: UITextField!
    @IBOutlet weak var clubTxtField: UITextField!
    @IBOutlet weak var membershipTypeTxtField: UITextField!
    @IBOutlet weak var mantenimientoTxtField: UITextField!
    @IBOutlet weak var ageTxtField: UITextField!
    @IBOutlet weak var heightTxtField: UITextField!
    @IBOutlet weak var weightTxtField: UITextField!
    @IBOutlet weak var mailTxtField: UITextField!
    var currentTextField: UITextField?
    
    var profileImageURL = APIManager.sharedInstance.profileImage
    var user = APIManager.sharedInstance.name
    var mailText = APIManager.sharedInstance.mail
    var membresia = APIManager.sharedInstance.memberNumber
    var club = APIManager.sharedInstance.club
    var tipoMembresia = APIManager.sharedInstance.member_type
    var mantenimiento = APIManager.sharedInstance.mainteiment
    var edad  = APIManager.sharedInstance.age
    var altura = APIManager.sharedInstance.height
    var peso = APIManager.sharedInstance.weight
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if user == nil {
            username.text = ""
         } else {
            username.text = user
            print("jdsjdsjiosdvjdsvoivjodi", username.text!)
          }
        if membresia == nil {
            membershipTxtField.text = ""
        } else {
            membershipTxtField.text = String(describing: membresia!)
        }
        if club == nil {
            clubTxtField.text = ""
        } else {
            clubTxtField.text = club
        }
        if tipoMembresia == nil {
            membershipTypeTxtField.text = ""
        } else {
            membershipTypeTxtField.text = tipoMembresia
        }
        if mantenimiento == nil {
            mantenimientoTxtField.text = ""
        } else {
            mantenimientoTxtField.text = mantenimiento
        }
        if edad == nil {
            ageTxtField.text = ""
        } else {
            ageTxtField.text = String(describing: edad!)
            age = Int(ageTxtField.text!)

        }
        if altura == nil {
            heightTxtField.text = ""
        } else {
            heightTxtField.text = altura
            height = Double(heightTxtField.text!)
        }
        if peso == nil {
            weightTxtField.text = ""
        } else {
            weightTxtField.text = peso
            weight = Double(weightTxtField.text!)
        }
        
        if mailText == nil {
          mailTxtField.text = ""
         } else {
           mailTxtField.text = mailText
    }
      
}
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextField?.resignFirstResponder()
        textField.resignFirstResponder()
        currentTextField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if (textField ==  ageTxtField) {
            heightTxtField.becomeFirstResponder()
        }  else if (textField == heightTxtField) {
            weightTxtField.becomeFirstResponder()
        } else if (textField ==  weightTxtField) {
           currentTextField?.resignFirstResponder()
    }
        return true
    }

}
