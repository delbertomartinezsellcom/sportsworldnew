//
//  BeneficiosViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/5/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class BeneficiosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var arrayOfImages = APIManager.sharedInstance.logotipoConvenio
    var arrayOfNames = APIManager.sharedInstance.nombreConvenio
    var arrayOfClausulas = APIManager.sharedInstance.clausulasConvenio
    
    

    private let cell = "ConveniosTableViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()

    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! ConveniosTableViewCell
        
        //cel.imageToShow.image = arrayOfImages[indexPath.row]
        cel.labelDescription.text = arrayOfNames[indexPath.row]
        
         if let decodedData = Data(base64Encoded:arrayOfImages[indexPath.row], options: .ignoreUnknownCharacters) {
             let newImage = UIImage(data: decodedData)
            var newArrayImages = [UIImage]()
            newArrayImages.append(newImage!)
            cel.imageToShow.image = newArrayImages[indexPath.row]
        }
        
        return cel 
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 258
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailConveniosViewController") as? DetailConveniosViewController
        vc?.descripcionClausula = arrayOfClausulas[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
   

}
