//
//  PasesRedimidosViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/7/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class PasesRedimidosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var arrayOfProducto = APIManager.sharedInstance.productoPaseInvitado
    var arrayCuando = APIManager.sharedInstance.cuandoPaseInvitado
  
    var arrayOfVigencias = ["12/12/2012","12/12/2012","12/12/2012"]
    
    private let cell = "PasesRedimidosTableViewCell"

    override func viewDidLoad() {
        super.viewDidLoad()
      
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        arrayOfProducto.removeAll()
        arrayCuando.removeAll()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfProducto.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! PasesRedimidosTableViewCell
        cel.tituloPase.text = arrayOfProducto[indexPath.row]
        cel.vigenciaPase.text = arrayCuando[indexPath.row]
        return cel
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
}
