//
//  PasesDeInvitadoViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/7/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit


class PasesDeInvitadoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    private let cell = "PasesDeInvitadoTableViewCell"

    var arrayOfPasesDeInvitado = APIManager.sharedInstance.nombrePase
   
    var arrayOfVigencias = APIManager.sharedInstance.finVigencia
    override func viewDidLoad() {
        super.viewDidLoad()

        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
        containerView.isHidden = true 
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfPasesDeInvitado.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! PasesDeInvitadoTableViewCell
        cel.numberOfPass.text = arrayOfPasesDeInvitado[indexPath.row]
        cel.vigencia.text! = arrayOfVigencias[indexPath.row]
        
        return cel 
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "EnviaPaseViewController") as? EnviaPaseViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func clickSegmentedControl(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            containerView.isHidden = true
           tabView.isHidden = false
        } else if sender.selectedSegmentIndex == 1 {
            containerView.isHidden = false
            tabView.isHidden = true 
        }
    }
    
}
