//
//  MenuViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 27/12/17.
//  Copyright © 2017 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import WatchKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var viewToShow: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    var profileImageURL = SavedData.getTheProfilePic()
    var profileName = APIManager.sharedInstance.name
    var nombres = APIManager.sharedInstance.nombreConvenio
    //////////CONECTA LOS OUTLETS DEL BOTÓN DEL MENÚ//////////
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          var nombres = APIManager.sharedInstance.nombreConvenio
        viewToShow.isHidden = true
        var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        profileImage.frame = CGRect(x: self.view.frame.width / 2 - 40 , y: 100, width: 80, height: 80)
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        profileImage.contentMode = .scaleAspectFill
        
       
    
 
        
      //  getThePendingCharges()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //profileImage.layer.cornerRadius = 100
        
        
        if profileImageURL == nil {
            profileImage.image = #imageLiteral(resourceName: "back_button")
        } else {
            let url = URL(string: profileImageURL)
            let data = try? Data(contentsOf: url!)
            profileImage.image = UIImage(data: data!)
        }
        if userName == nil {
            userName.text = "No disponilbe"
        } else {
            userName.text = profileName
        }
    
    }
    
    //MARK:- BOTONES:
    
    @IBAction func clickConfiguracionButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "ConfiguracionViewController") as! ConfiguracionViewController
        self.navigationController!.pushViewController(VC1, animated: true)
    }
    
 
    
   

    func getThePendingCharges(){
        APIManager.sharedInstance.getPendingCharges(onSuccess: { json in
            DispatchQueue.main.async {
                if APIManager.sharedInstance.status == true {
                    
                    print("Success Request GetCharges")
                }
            }
            
        }, onFailure: { error in
            
        })
    }
    
}
