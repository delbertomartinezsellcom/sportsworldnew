//
//  RutinasViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/12/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import PlayerKit
import AVKit
import MediaPlayer
class RutinasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate {
    var player = AVPlayer()
    private let cell = "RutinasCell"
    var thereIsCellTapped = false
    var selectedRowIndex = -1

    //MARK:- CONECTA LOS OUTLETS
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inbodyContainer: UIView!
    @IBOutlet weak var avancesContainer: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var instructionsView: UIView!
     var nameOfDays: [String] = ["LUN", "MAR", "MIE", "JUE", "VIE", "SAB","DOM"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        let editButton   = UIBarButtonItem(title: "Post",  style: .plain, target: self, action: #selector(didTap))
        
        self.navigationItem.rightBarButtonItem = editButton
        
        avancesContainer.isHidden = true
        inbodyContainer.isHidden = true
        //Customiza la navigatonBar
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
 
   //CONFIGURA LAS VISTAS.
        instructionsView.layer.borderColor = UIColor.gray.cgColor
        instructionsView.layer.borderWidth = 0.5
        self.tabView.rowHeight = 100
        

        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .selected)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        segmentedControl.selectedSegmentIndex = 1

        let editButton   = UIBarButtonItem(title: "Post",  style: .plain, target: self, action: #selector(didTap))
        
        self.navigationItem.rightBarButtonItem = editButton
        
    }
    @objc func didTap() {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "AgendaInbodyViewController") as! AgendaInbodyViewController
        self.navigationController!.pushViewController(VC1, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameOfDays.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! RutinasCell
    
        
        let embedHTML="<html><head><style type=\"text/css\">body {background-color: transparent;color: black;}</style><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes\"/></head><body style=\"margin:0\"><div><iframe src=\"//player.vimeo.com/video/139785390?autoplay=1&amp;title=1&amp;byline=1&amp;portrait=0\" width=\"640\" height=\"360\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div></body></html>"
        cel.videoView.scrollView.isScrollEnabled = true
        cel.videoView.scrollView.bounces = false
        cel.videoView.allowsInlineMediaPlayback = true
        cel.videoView.mediaPlaybackRequiresUserAction = true
        cel.videoView.loadHTMLString(embedHTML, baseURL: Bundle.main.bundleURL)
        let url: NSURL = NSURL(string: "https://vimeo.com/266224967")!
        cel.videoView.loadHTMLString(embedHTML as String, baseURL:url as URL )
        cel.videoView.resizeWebContent()
        cel.videoView.scrollView.delegate = self
        

        return cel
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == selectedRowIndex && thereIsCellTapped {
            return 320
        }
        
        return 100
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let screenSiz: CGRect = UIScreen.main.bounds
        let cell = RutinasCell()
        // avoid paint the cell is the index is outside the bounds
        if self.selectedRowIndex != -1 {
            
        }
        
        if selectedRowIndex != indexPath.row {
            self.thereIsCellTapped = true
            if screenSiz == CGRect(x: 0.0, y: 0.0, width: 375.0, height: 667.0) {
                
                print("puta screen size", screenSiz)

            }
            self.selectedRowIndex = indexPath.row
        }
        else {
            // there is no cell selected anymore
            self.thereIsCellTapped = false
            self.selectedRowIndex = -1
        }
        player.play()
        self.tabView.beginUpdates()
        self.tabView.endUpdates()
    }
/////////////////////ACCIONES DE LOS BOTONES///////////////////////
    @IBAction func clickSegmented(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            avancesContainer.isHidden = true
            inbodyContainer.isHidden = false
            tabView.isHidden = true
            titleLabel.text = "FITNESS TEST"
            instructionsView.isHidden = true
        } else if sender.selectedSegmentIndex == 1 {
            avancesContainer.isHidden = true
            inbodyContainer.isHidden = true
            tabView.isHidden = false
            instructionsView.isHidden = false
            titleLabel.text = APIManager.sharedInstance.name
        } else if sender.selectedSegmentIndex == 2 {
            avancesContainer.isHidden = false
            inbodyContainer.isHidden = true 
            tabView.isHidden = true
            instructionsView.isHidden = true
             titleLabel.text = "FITNESS TEST"
        }
    }


    
}
