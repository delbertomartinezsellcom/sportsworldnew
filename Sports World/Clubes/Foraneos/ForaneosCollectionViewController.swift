//
//  ForaneosCollectionViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 17/01/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

private let cell = "ForaneosCollectionViewCell"

class ForaneosCollectionViewController: UICollectionViewController {
    
    var stringArray: [String] = ["No hay información para mostrar", "No hay información para mostrar", "No hay información para mostrar", "No hay información para mostrar"]
    var arrayLocalClubes : [Club] = [Club]()
    override func viewDidLoad() {
        super.viewDidLoad()
        APIManager.sharedInstance.ciudadesVC = self
        arrayLocalClubes = APIManager.sharedInstance.allClubes.filter({$0.group == "Cerca de mi"})
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayLocalClubes.count
        
    }
    func update(value: String){
        if(value == ""){
            arrayLocalClubes = APIManager.sharedInstance.allClubes.filter({$0.group == "Cerca de mi"})
        }else{
            arrayLocalClubes = APIManager.sharedInstance.allClubes.filter({$0.group == "Cerca de mi"}).filter({$0.name.uppercased().contains(value.uppercased())})
        }
        self.collectionView?.reloadData()
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: cell, for: indexPath as IndexPath)
        if let theLabel = cel.viewWithTag(100) as? UILabel {
            theLabel.text = arrayLocalClubes[indexPath.row].name
        }
        return cel
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        APIManager.sharedInstance.selectedClub = arrayLocalClubes[indexPath.row]
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
}
