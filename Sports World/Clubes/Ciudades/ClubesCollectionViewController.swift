//
//  ClubesCollectionViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 17/01/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

private let cell = "CiudadesCollectionViewCell"

class ClubesCollectionViewController: UICollectionViewController {
    
    var stringArray: [String] = ["No hay información para mostrar", "No hay información para mostrar", "No hay información para mostrar", "No hay información para mostrar"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stringArray.count
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: cell, for: indexPath as IndexPath)
        
        return cel
    }
    /*override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     APIManager.sharedInstance.selectedClub = arrayLocalClubes[indexPath.row]
     let _ = self.navigationController?.popViewController(animated: true)
     }*/
    
}
