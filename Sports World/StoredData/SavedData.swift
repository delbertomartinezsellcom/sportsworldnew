//
//  SavedData.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 5/9/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import Foundation

public class SavedData {
    //GUARDA EN USER DEFAULTS LOS DATOS MAS USADOS DENTRO DE LA APP.
    class func setTheLongitude(double:Double) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(double, forKey:"setTheLongitude")
        userDefaults.synchronize()
    }
    
    class func getTheLongitude() -> Double {
        let userDefaults = UserDefaults.standard
        if let longitude = userDefaults.object(forKey: "setTheLongitude") as! Double? {
            return longitude
            
        } else {
            return 0.0
        }
    }

    class func setTheLatitude(double:Double) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(double, forKey:"setTheLatitude")
        userDefaults.synchronize()
    }
    
    class func getTheLatitude() -> Double {
        let userDefaults = UserDefaults.standard
        if let latitude = userDefaults.object(forKey: "setTheLatitude") as! Double? {
            return latitude
            
        } else {
            return 0.0
        }
    }
    
    class func setTheUserId(userId:Int) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(userId, forKey:"userId")
        userDefaults.synchronize()
    }
    
    class func getTheUserId() -> Int {
        let userDefaults = UserDefaults.standard
        if let userId = userDefaults.object(forKey: "userId") as! Int? {
            return userId
            
        } else {
            return 0
        }
    }
    
    class func settokenConekta(tokenConekta: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(tokenConekta, forKey:"tokenConekta")
        userDefaults.synchronize()
    }
    
    class func gettokenConekta() -> String {
        let userDefaults = UserDefaults.standard
        if let tokenConekta = userDefaults.object(forKey: "tokenConekta") as! String? {
            return tokenConekta
            
        } else {
            return ""
        }
    }
    
    class func setSecretKey(secretKey: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(secretKey, forKey:"secretKey")
        userDefaults.synchronize()
    }
    
    class func getSecretKey() -> String {
        let userDefaults = UserDefaults.standard
        if let secretKey = userDefaults.object(forKey: "secretKey") as! String? {
            return secretKey
            
        } else {
            return ""
        }
    }
    
    class func setTheName(theName: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(theName, forKey:"theName")
        userDefaults.synchronize()
    }
    
    class func getTheName() -> String {
        let userDefaults = UserDefaults.standard
        if let theName = userDefaults.object(forKey: "theName") as! String? {
            return theName
            
        } else {
            return ""
        }
    }
    class func setTheProfilePic(profilePic: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(profilePic, forKey:"profilePic")
        userDefaults.synchronize()
    }
    
    class func getTheProfilePic() -> String {
        let userDefaults = UserDefaults.standard
        if let profilePic = userDefaults.object(forKey: "profilePic") as! String? {
            return profilePic
            
        } else {
            return ""
        }
    }
}
